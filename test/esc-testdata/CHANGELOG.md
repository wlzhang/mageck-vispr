# Change Log

All notable changes to this project will be documented in this file.

## [0.5.0] - 2016-03-08
### Added
- Add possibility to correct for batch effects via sva/combat.
